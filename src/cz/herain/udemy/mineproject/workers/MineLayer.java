package cz.herain.udemy.mineproject.workers;

import cz.herain.udemy.mineproject.constants.Constants;
import cz.herain.udemy.mineproject.view.Board;

import java.util.Random;

/**
 * @author Vit Herain
 */
public class MineLayer implements Runnable {

    private int id;
    private Board board;

    public MineLayer(final int id, final Board board) {
        this.id = id;
        this.board = board;
    }

    @Override
    public void run() {
        Random random = new Random();

        while (true) {
            int index = random.nextInt(Constants.BOARD_ROWS * Constants.BOARD_COLUMNS);
            board.setMine(index);

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "" + id;
    }
}
