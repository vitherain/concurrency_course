package cz.herain.udemy.mineproject.view;

import cz.herain.udemy.mineproject.constants.State;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Vit Herain
 */
public class Cell extends JPanel {

    private int id;
    private Lock lock;
    private State state;
    private boolean hasBomb;

    public Cell(int id) {
        initVariables(id);
        setLayout(new GridLayout());
    }

    private void initVariables(final int id) {
        this.id = id;
        this.lock = new ReentrantLock();
        this.state = State.EMPTY;
        this.hasBomb = false;
    }

    public void lock() {
        try {
            lock.tryLock(10, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void unlock() {
        lock.unlock();
    }

    @Override
    public String toString() {
        return "" + id +
                ", " + state.toString() +
                ", " + hasBomb;
    }
}
