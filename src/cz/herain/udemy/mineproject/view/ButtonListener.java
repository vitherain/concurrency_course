package cz.herain.udemy.mineproject.view;

/**
 * @author Vit Herain
 */
public interface ButtonListener {

    void startClicked();
    void stopClicked();
}
