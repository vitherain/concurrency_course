package cz.herain.udemy.mineproject.app;

import cz.herain.udemy.mineproject.constants.Constants;
import cz.herain.udemy.mineproject.view.Board;
import cz.herain.udemy.mineproject.view.ButtonListener;
import cz.herain.udemy.mineproject.view.Toolbar;
import cz.herain.udemy.mineproject.workers.MineLayer;
import cz.herain.udemy.mineproject.workers.MineSweeper;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Vit Herain
 */
public class MainFrame extends JFrame implements ButtonListener {

    private Toolbar toolbar;
    private Board board;
    private ExecutorService layerExecutor;
    private ExecutorService sweepersExecutor;
    private MineLayer[] mineLayers;
    private MineSweeper[] mineSweepers;

    public MainFrame() {
        super(Constants.APP_NAME);

        toolbar = new Toolbar();
        board = new Board();

        initializeVariables();

        toolbar.setButtonListener(this);

        add(toolbar, BorderLayout.NORTH);
        add(board, BorderLayout.CENTER);

        setSize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initializeVariables() {
        mineLayers = new MineLayer[Constants.NUMBER_OF_LAYERS];
        mineSweepers = new MineSweeper[Constants.NUMBER_OF_SWEEPERS];
    }

    @Override
    public void startClicked() {
        this.layerExecutor = Executors.newFixedThreadPool(Constants.NUMBER_OF_LAYERS);
        this.sweepersExecutor = Executors.newFixedThreadPool(Constants.NUMBER_OF_SWEEPERS);

        try {
            for (int i = 0 ; i < Constants.NUMBER_OF_LAYERS ; i++) {
                mineLayers[i] = new MineLayer(i, board);
                layerExecutor.execute(mineLayers[i]);
            }
            for (int i = 0 ; i < Constants.NUMBER_OF_SWEEPERS ; i++) {
                mineSweepers[i] = new MineSweeper(i, board);
                sweepersExecutor.execute(mineSweepers[i]);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            layerExecutor.shutdown();
            sweepersExecutor.shutdown();
        }
    }

    @Override
    public void stopClicked() {
        System.gc();
        System.exit(0);
    }
}
