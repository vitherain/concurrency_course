package cz.herain.udemy.mineproject.constants;

/**
 * @author Vit Herain
 */
public enum State {

    EMPTY,
    MINE,
    MINESWEEPER,
    MINELAYER
}
