package cz.herain.udemy.mineproject.constants;

/**
 * @author Vit Herain
 */
public final class Constants {

    public static final int NUMBER_OF_SWEEPERS = 5;
    public static final int NUMBER_OF_LAYERS = 5;
    public static final int BOARD_ROWS = 10;
    public static final int BOARD_COLUMNS = 10;
    public static final int BOARD_WIDTH = 800;
    public static final int BOARD_HEIGHT = 650;
    public static final String APP_NAME = "Mine simulation!";

    private Constants() {
    }
}
