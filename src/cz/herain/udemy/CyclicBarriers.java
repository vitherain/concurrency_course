package cz.herain.udemy;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Vit Herain
 */
public class CyclicBarriers {

    public static void main(String... args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        CyclicBarrier barrier = new CyclicBarrier(5, () -> System.out.println("We are able to use the trained neural network..."));

        for (int i = 0 ; i < 5 ; i++) {
            executorService.execute(new WorkerCB(i+1, barrier));
        }

        executorService.shutdown();
    }
}

class WorkerCB implements Runnable {

    private int id;
    private Random random;
    private CyclicBarrier cyclicBarrier;

    public WorkerCB(final int id, final CyclicBarrier cyclicBarrier) {
        this.id = id;
        this.cyclicBarrier = cyclicBarrier;
        random = new Random();
    }

    @Override
    public void run() {
        doWork();
    }

    private void doWork() {
        System.out.println("Thread with ID " + id + " starts the task...");
        try {
            Thread.sleep(random.nextInt(3000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Thread with ID " + id + " finished");

        try {
            cyclicBarrier.await();
            System.out.println("After await in thread with ID " + id + "...");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "WorkerCB{" +
                "id=" + id +
                ", random=" + random +
                ", cyclicBarrier=" + cyclicBarrier +
                '}';
    }
}