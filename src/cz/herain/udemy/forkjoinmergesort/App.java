package cz.herain.udemy.forkjoinmergesort;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class App {

    public static void main(String... args) {
        int[] nums = initializeNums();

        SequentialMergeSort sequentialMergeSort = new SequentialMergeSort();

        long start = System.currentTimeMillis();
        sequentialMergeSort.mergeSort(nums);
        System.out.println("Sequential algorithm: " + (System.currentTimeMillis() - start) + " ms");

        ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        ParallelMergeSortTask parallelMergeSortTask = new ParallelMergeSortTask(nums);

        start = System.currentTimeMillis();
        pool.invoke(parallelMergeSortTask);
        System.out.println("Parallel algorithm: " + (System.currentTimeMillis() - start) + " ms");
    }

    public static int[] initializeNums() {
        Random random = new Random();

        int[] nums = new int[100000000];

        for (int i = 0 ; i < 100000000 ; i++) {
            nums[i] = random.nextInt(1000);
        }

        return nums;
    }
}
