package cz.herain.udemy.diningphilosopher;

/**
 * @author Vit Herain
 */
public enum State {

    LEFT,
    RIGHT;
}
