package cz.herain.udemy;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author Vit Herain
 */
public class PriorityQueues {

    public static void main(String... args) {
        BlockingQueue<Person> queue = new PriorityBlockingQueue<>();

        new Thread(new FirstWrkr(queue)).start();
        new Thread(new SecondWrkr(queue)).start();
    }
}

class Person implements Comparable<Person> {

    private int age;
    private String name;

    public Person(final int age, final String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public int compareTo(final Person o) {
        return name.compareTo(o.getName());
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

class FirstWrkr implements Runnable {

    private BlockingQueue<Person> blockingQueue;

    public FirstWrkr(final BlockingQueue<Person> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            blockingQueue.put(new Person(12, "Adam"));
            blockingQueue.put(new Person(45, "Joe"));
            blockingQueue.put(new Person(78, "Daniel"));
            Thread.sleep(1000);
            blockingQueue.put(new Person(32, "Noel"));
            Thread.sleep(1000);
            blockingQueue.put(new Person(34, "Kevin"));
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

class SecondWrkr implements Runnable {

    private BlockingQueue<Person> blockingQueue;

    public SecondWrkr(final BlockingQueue<Person> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            System.out.println(blockingQueue.take());
            Thread.sleep(1000);
            System.out.println(blockingQueue.take());
            Thread.sleep(1000);
            System.out.println(blockingQueue.take());
            System.out.println(blockingQueue.take());
            System.out.println(blockingQueue.take());
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
