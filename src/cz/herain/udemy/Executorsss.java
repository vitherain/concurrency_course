package cz.herain.udemy;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Vit Herain
 */
public class Executorsss {

    public static void main(String... args) {
        ExecutorService executorService = Executors.newCachedThreadPool();

        for (int i = 0 ; i < 5 ; i++) {
            executorService.execute(new Workerrrr());
        }
    }
}

class Workerrrr implements Runnable {

    @Override
    public void run() {
        for (int i = 0 ; i < 10 ; i ++) {
            System.out.println(i);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}