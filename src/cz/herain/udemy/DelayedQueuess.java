package cz.herain.udemy;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author Vit Herain
 */
public class DelayedQueuess {

    public static void main(String... args) {
        BlockingQueue<DelayedWorker> queue = new DelayQueue<>();

        try {
            queue.put(new DelayedWorker(1000, "This is the first message..."));
            queue.put(new DelayedWorker(10000, "This is the second message..."));
            queue.put(new DelayedWorker(4000, "This is the third message..."));
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        while (!queue.isEmpty()) {
            try {
                System.out.println(queue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class DelayedWorker implements Delayed {

    private long duration;
    private String message;

    public DelayedWorker(final long duration, final String message) {
        this.duration = System.currentTimeMillis() + duration;
        this.message = message;
    }

    @Override
    public long getDelay(final TimeUnit unit) {
        return unit.convert(duration - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(final Delayed o) {
        if (this.duration < ((DelayedWorker)o).getDuration()) {
            return -1;
        }
        if (this.duration > ((DelayedWorker)o).getDuration()) {
            return +1;
        }

        return 0;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(final long duration) {
        this.duration = duration;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "DelayedWorker{" +
                "duration=" + duration +
                ", message='" + message + '\'' +
                '}';
    }
}