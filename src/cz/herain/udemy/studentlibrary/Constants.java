package cz.herain.udemy.studentlibrary;

/**
 * @author Vit Herain
 */
public final class Constants {

    private Constants() {
    }

    public static final int NUMBER_OF_STUDENTS = 5;
    public static final int NUMBER_OF_BOOKS = 7;
}
