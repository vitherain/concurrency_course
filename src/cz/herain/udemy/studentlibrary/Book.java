package cz.herain.udemy.studentlibrary;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Vit Herain
 */
public class Book {

    private int id;
    private Lock lock;

    public Book(final int id) {
        this.id = id;
        this.lock = new ReentrantLock();
    }

    public void read(Student student) throws InterruptedException {
        System.out.println(student + " wants to read " + this);
        if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
            System.out.println(student + " starts reading " + this);
            Thread.sleep(2000);
            lock.unlock();
            System.out.println(student + " has finished reading " + this);
            return;
        }
        System.out.println(student + " was not able to read " + this);
    }

    @Override
    public String toString() {
        return "Book #" + id;
    }
}
